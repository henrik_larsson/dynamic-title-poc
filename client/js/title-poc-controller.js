(function (angular) {
    angular.module('titlePoc')
        .controller('TitleController', ['$scope', 'TitleService', titleController]);

    function titleController($scope, titleService) {
        $scope.titleService = titleService;

        $scope.$on('$routeChangeSuccess', function() {
            titleService.setTitle();
        });
    }
})(angular);