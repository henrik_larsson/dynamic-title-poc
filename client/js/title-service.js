(function (angular) {
    angular.module('titlePoc').factory('TitleService', ['$http', '$location', titleService]);

    var currentTitle;
    var titles = {};
    function titleService($http, $location) {
        var service = {
            getTitle: function() { return currentTitle; },
            setTitle: setTitle
        }

        $http.get('/meta').then(function(metaData) {
            titles = metaData.data;
            service.setTitle();
        });

        return service;


        function setTitle() {
            if (titles[$location.path()]) {
                currentTitle = titles[$location.path()].title;
            }
        }
    }
})(angular);