(function(angular) {
    var titlePocApp = angular.module('titlePoc', ['ngRoute']);

    titlePocApp.config(['$routeProvider', '$locationProvider', setupRoutes]);

    function setupRoutes($routeProvider, $locationProvider) {
        $locationProvider.html5Mode(true);

        $routeProvider
            .when('/home', {
                templateUrl: 'partials/home'
            })
            .when('/away', {
                templateUrl: 'partials/away'
            })
            .when('/', {
                templateUrl: 'partials/main'
            });
    }
})(angular);