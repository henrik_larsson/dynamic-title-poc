var express = require('express');

var server = express();
server.set('views', 'server/views');
server.set('view engine', 'jade');

server.use('/libs', express.static('client/libs'));
server.use('/js', express.static('client/js'));

server.get('/meta/:id', serveMetaData);
server.get('/meta', serveMetaData);

server.get('/partials/:id', function(request, response) {
    response.render(request.params.id);
});

server.get('*', function(request, response) {
    response.render('index');
});

server.listen(1337);

console.log('Listening on port 1337...');

// Handlers

function serveMetaData(request, response) {
    var metaInfo = {
        '/': {
            title: 'This is Main'
        },
        '/home': {
            title: 'This is Home'
        },
        '/away': {
            title: 'This is Away'
        }
    };

    if (!request.params.id) {
        response.json(metaInfo);
    } else if (metaInfo[request.params.id]) {
        response.json(metaInfo[request.params.id]);
    } else {
        response.status(404).send({ error: 'Does not exist' });
    }
}